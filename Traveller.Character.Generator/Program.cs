﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Traveller.Database;
using Traveller.Character.Sheet;

namespace Traveller.Character.Generator
{
    static class Program
    {
        static void Main(string[] args)
        {
            using (var repo = new Traveller.Database.Repositories.Character.Logic.Skills())
            {
                foreach (var value in repo.GetSkills())
                {
                    Console.WriteLine("{0}: {1}", value.Name, String.Join(",", value.Specialities));
                }
            }

            Console.ReadLine();
        }
    }
}
