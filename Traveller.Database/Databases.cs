﻿using System;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using System.Collections.Generic;

using MyCouch;
using MyCouch.Requests;

namespace Traveller.Database
{

    public static class Databases
    {
        private static string mDatabaseAddress;

        static Databases()
        {
            // Get the database address
            mDatabaseAddress = ConfigurationManager.AppSettings["databaseConnection"];

            // Emsure the database exists, if not creates it
            GetClient().Database.PutAsync().Wait();
        }

        public static MyCouchClient GetClient()
        {
            return new MyCouchClient(mDatabaseAddress);
        }
    }
}
