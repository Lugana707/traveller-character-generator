﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using MyCouch;

namespace Traveller.Database.Repositories
{
    public abstract class Repository<T> : IDisposable
        where T : class
    {
        protected MyCouchClient Database { get; private set; }

        public Repository()
        {
            Database = ConnectToDatabase();
        }

        protected abstract MyCouchClient ConnectToDatabase();

        protected T GetValue(string id)
        {
            var action = Database.Entities.GetAsync<T>(id);
            action.Wait();
            return action.Result.Content;
        }

        public virtual void Dispose()
        {
            Database.Dispose();
        }
    }
}
