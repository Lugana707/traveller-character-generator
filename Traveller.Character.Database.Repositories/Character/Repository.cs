﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Traveller.Database;

namespace Traveller.Database.Repositories.Character
{
    public abstract class Repository<T> : Traveller.Database.Repositories.Repository<T>
        where T : class
    {
        protected override MyCouch.MyCouchClient ConnectToDatabase()
        {
            return Databases.GetClient();
        }
    }
}
