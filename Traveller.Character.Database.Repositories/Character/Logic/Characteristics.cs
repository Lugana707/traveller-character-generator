﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using Traveller.Database;

namespace Traveller.Database.Repositories.Character.Logic
{
    public class Characteristics : Repository<Characteristics.Characteristic>
    {
        private const string Id = "characteristics";

        public class Characteristic
        {
            public string Id { get; set; }
            public string[] Names { get; set; }
            public int[] Modifiers { get; set; }
        }

        public string[] GetCharacteristics()
        {
            return GetValue(Id).Names;
        }

        public int[] GetModifiers()
        {
            return GetValue(Id).Modifiers;
        }
    }
}
