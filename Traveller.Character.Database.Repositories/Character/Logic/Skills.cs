﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Traveller.Database.Repositories.Character.Logic
{
    public class Skills : Repository<Skills.SkillStore>
    {
        public class Skill
        {
            public string Name { get; set; }
            public string[] Specialities { get; set; }
        }

        public class SkillStore
        {
            public string Id { get; set; }
            public Skill[] Skills { get; set; }
        }

        public Skill[] GetSkills()
        {
            return GetValue("skills").Skills;
        }
    }
}
