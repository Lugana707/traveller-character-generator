﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Traveller.Database.Repositories.Character.Logic
{
    public class Planets : Repository<Planets.PlanetStore>
    {
        public class Planet
        {
            public string Name { get; set; }
            public string Skill { get; set; }
        }

        public class PlanetStore
        {
            public string Id { get; set; }
            public Planet[] Planets { get; set; }
        }

        public Planet[] GetPlanets()
        {
            return GetValue("planets").Planets;
        }
    }
}
