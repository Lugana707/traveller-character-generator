﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Armour : Equipment
    {
        public int Rating { get; set; }
        public string Notes { get; set; }
    }
}
