﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Event
    {
        public string Description { get; set; }

        public string[] Allies { get; set; }
        public string[] Contacts { get; set; }
        public string[] Rivals { get; set; }
    }
}
