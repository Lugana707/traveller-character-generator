﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Career
    {
        // Variables 

        public string Name { get; set; }

        public CareerReward[] PersonalDevelopment { get; set; }
        public CareerReward[] ServiceSkill { get; set; }
        public CareerReward[] AdavancedEducation { get; set; }

        public Specialisation[] Specilisations { get; set; }
        public string Branch { get; set; }
        public Event[] Events { get; set; }
        public int Rank { get; set; }
        public string Title { get; set; }
    }
}
