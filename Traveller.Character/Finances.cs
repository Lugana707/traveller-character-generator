﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Finances
    {
        //Variables

        public int Pension { get; set; }
        public int Debt { get; set; }
        public int CashOnHand { get; set; }
        public int MonthlyShipPayment { get; set; }
    }
}
