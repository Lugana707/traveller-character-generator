﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Specialisation
    {
        // Variables

        public string Name { get; set; }
        public int Survival { get; set; }
        public int Advancement { get; set; }
        public string[] Training { get; set; }
    }
}
