﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Characteristic
    {
        // Variables

        public string Name { get; set; }
        public int Value { get; set; }
    }
}
