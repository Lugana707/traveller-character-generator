﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Skill
    {
        // Variables

        public string Name { get; set; }
        public string Detail { get; set; }
        public int Proficiency { get; set; }

    }
}
