﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class EquipmentCollection
    {
        // Variables

        public Equipment[] Equipment { get; set; }
        public int TotalMass
        {
            get
            {
                int total = 0;
                foreach (Equipment item in Equipment)
                {
                    total += item.Mass;
                }
                return total;
            }
        }
    }
}
