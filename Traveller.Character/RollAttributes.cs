﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traveller.Utilities;

namespace Traveller.Character.Sheet
{
    public struct RollAttributes
    {
        //Variable 

        public int NumberOfDice { get; set; }

        public int GetDiceRoll()
        {
            return Dice.RollDice(NumberOfDice);
        }
    }
}
