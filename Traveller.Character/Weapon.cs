﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Weapon : Equipment
    {
        // Variables

        public string Range { get; set; }
        public int Recoil { get; set; }
        public int AmmoCost { get; set; }
        public int Capacity { get; set; }
        public RollAttributes Damage { get; set; }

        public int Personal { get; set; }
        public int Close { get; set; }
        public int Short { get; set; }
        public int Medium { get; set; }
        public int Long { get; set; }
        public int VeryLong { get; set; }
        public int Distant { get; set; }
    }
}
