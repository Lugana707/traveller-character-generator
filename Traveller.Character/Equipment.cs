﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Equipment
    {
        // Variables

        public string Name { get; set; }
        public int Mass { get; set; }
        public int TechLevel { get; set; }
        public int Cost { get; set; }
        
    }
}
