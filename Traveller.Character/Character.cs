﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Character.Sheet
{
    public class Character
    {
        // Variables

        public Characteristic[] Characteristics { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Race { get; set; }
        public string Homeworld { get; set; }
        public Skill[] Skills { get; set; }
        public EquipmentCollection Equipment { get; set; }
        public Finances Finanaces { get; set; }
        public Career Career { get; set; }

    }
}
