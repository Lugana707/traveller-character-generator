﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Utilities.Settings
{
    public static class Settings
    {
        // Variables

        private static ISettingProvider mConfigurationSettingProvider;

        public static ISettingProvider ConfigurationSettings
        {
            get
            {
                if (mConfigurationSettingProvider == null) 
                {
                    mConfigurationSettingProvider = new ConfigurationSettings();
                }
                return mConfigurationSettingProvider;
            }
        }
    }
}
