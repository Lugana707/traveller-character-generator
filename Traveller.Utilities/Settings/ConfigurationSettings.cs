﻿using System;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Traveller.Utilities.Settings
{
    public class ConfigurationSettings : ISettingProvider
    {
        // Accessors

        public string GetSetting(string name)
        {
            string settingValue = ConfigurationManager.AppSettings[name];

            if (settingValue == null)
            {
                return string.Empty;
            }

            return settingValue;
        }
    }
}
