﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traveller.Utilities.Settings
{
    public interface ISettingProvider
    {
        string GetSetting(string name);
    }
}
