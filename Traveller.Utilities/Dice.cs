﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Traveller.Utilities
{
    public static class Dice
    {
        // Variables

        private static Random mRandom;
        private static Stack<byte> mRandomNumbers;

        // Constructors

        static Dice()
        {
            mRandom = new Random();
            mRandomNumbers = new Stack<byte>();
            PopulateRandomNumbers();
        }

        // Events

        public static int RollDice(int numberOfDice = 2, int sides = 6)
        {
            int total = 0;

            if (mRandomNumbers.Count() < numberOfDice)
            {
                PopulateRandomNumbers();
            }

            for (int i = 0; i < numberOfDice; i++)
            {
                var roll = (int)Math.Ceiling((mRandomNumbers.Pop() / 255.0f) * sides);
                total += roll < 1 ? 1 : roll;
            }

            return total;
        }

        private static void PopulateRandomNumbers(int count = 255)
        {
            var numbers = new byte[count];
            mRandom.NextBytes(numbers);

            mRandomNumbers.Clear();
            foreach (var number in numbers)
            {
                mRandomNumbers.Push(number);
            }
        }
    }
}
